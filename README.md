#How to setup HOTflo

Make sure you setup a database connection and a mailer_user when installing this package.

```
git clone git@bitbucket.org:joostvandermeijden/hotflo.git
cd hotflo
composer install
php bin/console doctrine:schema:update --force
php bin/console doctrine:fixtures:load --no-interaction
```

You can run all tests with `./vendor/bin/simple-phpunit` to verify that everything is working.

Navigate to the homepage (f.e.: https://hotflo.dev) for further instructions.