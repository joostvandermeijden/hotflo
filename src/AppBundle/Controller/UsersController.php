<?php

namespace AppBundle\Controller;

use AppBundle\Enums\ProgrammingLevelEnum;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends Controller
{
    
    /**
     * @Route("/users/{filter_programming_level}", defaults={"filter_programming_level"=null}, name="users.overview")
     *
     * @param null|string                          $filter_programming_level
     * @param \AppBundle\Repository\UserRepository $userRepository
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(?string $filter_programming_level, UserRepository $userRepository)
    {
        if($filter_programming_level !== null) {
            $users = $userRepository->findByProgrammingLevel($filter_programming_level);
        } else {
            $users = $userRepository->findAll();
        }
        
        return $this->render('users/overview.html.twig', [
            'users'                  => $users,
            'programming_level_enum' => new ProgrammingLevelEnum(),
            'current_level'          => $filter_programming_level,
        ]);
    }
    
}