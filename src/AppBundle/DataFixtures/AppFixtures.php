<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use AppBundle\Enums\ProgrammingLevelEnum;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures implements ORMFixtureInterface
{
    
    /**
     * @var \Faker\Generator
     */
    protected $faker;
    
    /**
     * @var \Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface
     */
    private $password_encoder;
    
    public function __construct(UserPasswordEncoderInterface $password_encoder)
    {
        $this->faker = Factory::create();
        $this->password_encoder = $password_encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        for($i=0; $i<50; $i++) {
            $user = new User();
            
            $email = $this->faker->email;
            $user->setEmail($email);
            $user->setEmailCanonical($email);
            $user->setEnabled($this->faker->boolean(80));
            $user->setPlainPassword($this->password_encoder->encodePassword($user, $this->faker->password));
            $user->setLastLogin($this->faker->dateTimeBetween('-100 days', '-1 days'));
            
            $username = $this->faker->userName;
            $user->setUsername($username);
            $user->setUsernameCanonical($username);
            
            $user->setGender($this->faker->randomElement([
                User::GENDER_FEMALE,
                User::GENDER_MALE,
                User::GENDER_OTHER,
            ]));
            
            $user->setProgrammingLevel($this->faker->randomElement([
                ProgrammingLevelEnum::PROGRAMMING_LEVEL_JUNIOR,
                ProgrammingLevelEnum::PROGRAMMING_LEVEL_MEDIOR,
                ProgrammingLevelEnum::PROGRAMMING_LEVEL_SENIOR,
            ]));
            $manager->persist($user);
        };
        
        $manager->flush();
    }
}
