<?php

namespace AppBundle\Entity;

use AppBundle\Enums\ProgrammingLevelEnum;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    
    /** @deprecated  */
    const GENDER_FEMALE = 0;
    
    /** @deprecated  */
    const GENDER_MALE = 1;
    
    /** @deprecated  */
    const GENDER_OTHER = 2;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="gender", type="smallint", length=1)
     */
    private $gender = self::GENDER_OTHER;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="programming_level", type="smallint", length=1)
     */
    private $programming_level = ProgrammingLevelEnum::PROGRAMMING_LEVEL_JUNIOR;
    
    /**
     * @return string
     */
    public function getGenderTransKey()
    {
        return 'profile.gender_' . $this->gender;
    }
    
    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }
    
    /**
     * @param int $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }
    
    /**
     * todo: why do we need this underscore in the method name?..
     *
     * @return int
     */
    public function getProgramming_Level()
    {
        return $this->programming_level;
    }
    
    /**
     * @param int $programming_level
     */
    public function setProgrammingLevel($programming_level)
    {
        $this->programming_level = $programming_level;
    }
    
}