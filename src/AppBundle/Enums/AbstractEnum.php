<?php

namespace AppBundle\Enums;

abstract class AbstractEnum
{
    
    /**
     * @var array
     */
    protected $enums = [];
    
    /**
     * @return array
     */
    public function getList()
    {
        return $this->enums;
    }
    
    /**
     * @param $key
     * @return string
     */
    public function convertToPHPValue($key = null): string
    {
        $this->assertExistence($key, $this->enums);
        return $this->enums[ $key ];
    }
    
    /**
     * @param string $key
     *
     * @return int
     */
    public function convertToDatabaseValue(string $key): int
    {
        $flipped_enums = array_flip($this->enums);
        $this->assertExistence($key, $flipped_enums);
    
        return $flipped_enums[ $key ];
    }
    
    /**
     * @param $key
     * @param $array
     */
    private function assertExistence($key, $array)
    {
        if (!isset($array[ $key ])) {
            throw new \InvalidArgumentException('Invalid enum value ' . $key. ' for class' . get_called_class());
        }
    }
    
}