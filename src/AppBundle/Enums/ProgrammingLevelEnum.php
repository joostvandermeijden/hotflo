<?php

namespace AppBundle\Enums;

class ProgrammingLevelEnum extends AbstractEnum
{
    
    const PROGRAMMING_LEVEL_JUNIOR = 0;
    const PROGRAMMING_LEVEL_MEDIOR = 1;
    const PROGRAMMING_LEVEL_SENIOR = 2;
    
    protected $enums = [
        self::PROGRAMMING_LEVEL_JUNIOR => 'Junior',
        self::PROGRAMMING_LEVEL_MEDIOR => 'Medior',
        self::PROGRAMMING_LEVEL_SENIOR => 'Senior',
    ];
    
}