<?php

namespace AppBundle\Form;

use AppBundle\Services\UserService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    
    /**
     * @var \AppBundle\Services\UserService
     */
    protected $userService;
    
    /**
     * RegistrationType constructor.
     *
     * @param \AppBundle\Services\UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('gender',
            ChoiceType::class,
            ['choices' => $this->userService->getGenders(true)]
        );
    }
    
    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }
    
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
    
}