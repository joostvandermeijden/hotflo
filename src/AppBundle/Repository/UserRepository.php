<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }
    
    /**
     * @param string $level
     * @return array
     */
    public function findByProgrammingLevel(string $level)
    {
        return $this->createQueryBuilder('u')
            ->select('u.id, u.username, u.email, u.gender, u.programming_level')
            ->where('u.programming_level = :programming_level')
            ->setParameter('programming_level', $level)
            ->getQuery()
            ->getResult();
    }
    
}