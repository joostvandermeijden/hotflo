<?php

namespace AppBundle\Services;

use AppBundle\Entity\User;
use Symfony\Component\Translation\TranslatorInterface;


class UserService
{
    
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    protected $translator;
    
    /**
     * UserService constructor.
     *
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }
    
    /**
     * @param bool $flip
     * @return array
     */
    public function getGenders(bool $flip = false)
    {
        $genders = [
            User::GENDER_FEMALE,
            User::GENDER_MALE,
            User::GENDER_OTHER,
        ];
        
        $translated_genders = [];
        foreach($genders as $gender) {
            $translated_genders[ $gender ] = $this->translator->trans('profile.gender_' . $gender, [], 'FOSUserBundle');
        }
        
        if($flip === true) {
            return array_flip($translated_genders);
        }
        
        return $translated_genders;
    }
    
}