<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\UsersController;
use AppBundle\Enums\ProgrammingLevelEnum;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class UsersControllerTest extends WebTestCase
{
    
    private $users_tbody_selector = '#users-overview tbody';
    
    public function testListingAllUsers()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/users');
    
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Senior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertContains('Medior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertContains('Junior', $crawler->filter($this->users_tbody_selector)->text());
    }
    
    public function testListingSeniorUsers()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/users/' . ProgrammingLevelEnum::PROGRAMMING_LEVEL_SENIOR);
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Senior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertNotContains('Medior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertNotContains('Junior', $crawler->filter($this->users_tbody_selector)->text());
    }
    
    public function testListingMediorUsers()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/users/' . ProgrammingLevelEnum::PROGRAMMING_LEVEL_MEDIOR);
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotContains('Senior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertContains('Medior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertNotContains('Junior', $crawler->filter($this->users_tbody_selector)->text());
    }
    
    public function testListingJuniorUsers()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/users/' . ProgrammingLevelEnum::PROGRAMMING_LEVEL_JUNIOR);
        
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertNotContains('Senior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertNotContains('Medior', $crawler->filter($this->users_tbody_selector)->text());
        $this->assertContains('Junior', $crawler->filter($this->users_tbody_selector)->text());
    }
    
    public function testIndexActionWithoutFiltering()
    {
        $mockRepo = $this->createMock(UserRepository::class);
        $mockRepo->expects($this->once())
            ->method('findAll')
            ->willReturn([]);
        
        $usersController = new FakeUsersController();
        $usersController->indexAction(null, $mockRepo);
    }
    
    public function testIndexActionWithFiltering()
    {
        $mockRepo = $this->createMock(UserRepository::class);
        $mockRepo->expects($this->once())
            ->method('findByProgrammingLevel')
            ->with(ProgrammingLevelEnum::PROGRAMMING_LEVEL_JUNIOR)
            ->willReturn([]);
    
        $usersController = new FakeUsersController();
        $usersController->indexAction(ProgrammingLevelEnum::PROGRAMMING_LEVEL_JUNIOR, $mockRepo);
    }
    
}

/**
 * Class FakeUsersController
 *
 * I wanted to show that I'm also able to write unit tests
 * Unfortunately the Symfony Controllers' render() method
 * tries to fetch data from his container which is ofcourse
 * not available when initializing a new UsersController()
 *
 * To still make the tests I wrote a small FakeUsersController
 * at the bottom of this class which extends the UsersController
 * and override it's render method.
 */
class FakeUsersController extends UsersController
{
    
    protected function render($view, array $parameters = array(), Response $response = null)
    {
        // do nothing :)
    }
    
}
